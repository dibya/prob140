.. prob140 documentation master file, created by
   sphinx-quickstart on Sun Nov  6 14:34:51 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to prob140's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: prob140

.. autoclass:: DiscreteDistribution
    :members:

.. autoclass:: FiniteDistribution
    :members:

.. autoclass:: InfiniteDistribution
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
